val = [None] * 16

t0 = [None] * 16
t1 = [None] * 16

def a(fro, to1, to0, outp=None):
	val[fro] = outp
	t0[fro] = to0
	t1[fro] = to1

a(0,1,2)
a(1,3,4, 1)
a(2,5,6, 0)
a(3,7,8)
a(4,7,8, 1)
a(5,4,8, 0)
a(6,8,9)
a(7,13,0, 0)
a(8,10,11)
a(9,11,12, 0)
a(10,7,11)
a(11,13,12, 1)
a(12,13,14)
a(13,14,0, 0)
a(14,0,15, 1)
a(15,0,9)

def bin_to_ascii(bs):
	bsl = (len(bs)) // 8 * 8
	bs = bs.ljust(bsl, "0")
	cc = 0
	cp = 1
	s = ""
	for c in bs:
		cc = cc + int(c) * cp
		cp *= 2
		if cp == 256:
			s += chr(cc)
			cc = 0
			cp = 1
	return s
		
def check(bs):
	s = bin_to_ascii(bs)
	for c in s:
		if ord(c) < 32 or ord(c) > 126:
			return False
	return True 

import sys

def bkt(s, target, bs, S):
	S = S + [(s, val[s])]
	if len(bs)%8 == 0 and not check(bs): return
	if target == '':
		if check(bs + "0000000"):
			print(f"'{bin_to_ascii(bs + '0000000')}': {S}")
			sys.exit(0)
		return
	if val[s] == None:
		bkt(t0[s], target, bs+'0', S)
		bkt(t1[s], target, bs+'1', S)
	elif val[s] == int(target[0]):
		bkt(t0[s], target[1:], bs+'0', S)
		bkt(t1[s], target[1:], bs+'1', S)

bkt(0, input("Baby bear said: "), "", [])
