bits 64

global _start

section UPX0 exec nowrite

putbit:
mov r9, rsi

mov r10, rdi
mov rdi, your_output
movzx r8, byte [your_output_size]
add rdi, r8
stosb
inc byte [your_output_size]
mov rdi, r10

add al, 48
push qword 0
mov [rsp], al
xor rdi, rdi
mov rsi, rsp
add rsp, 8
xor ah, ah
div al
mov rdx, 1
syscall
dec byte [output_remaining]
jz done_encoding
mov rsi, r9
ret


n14_lodsb_before_calling:
movzx rax, al
push rax
mov al, 1
call putbit
cmp qword [rsp], 1
jz n0
n15:
lodsb
inc al
cmp al, 1
jz n9
call n0


banner: db 10, 32, 32, 40, 99, 41, 46, 45, 46, 40, 99, 41, 32, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 32, 32, 32, 32, 32, 32, 32, 226, 150, 136, 32, 32, 32, 32, 32, 32, 32, 32, 32, 226, 150, 136, 10, 32, 32, 32, 47, 32, 46, 95, 46, 32, 92, 32, 32, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 32, 32, 32, 32, 32, 32, 32, 226, 150, 136, 32, 32, 32, 32, 32, 32, 32, 32, 32, 226, 150, 136, 10, 32, 95, 95, 92, 40, 32, 89, 32, 41, 47, 95, 95, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 10, 40, 95, 46, 45, 47, 39, 45, 39, 92, 45, 46, 95, 41, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 10, 32, 32, 32, 124, 124, 32, 88, 32, 124, 124, 32, 32, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 32, 32, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 32, 226, 150, 136, 10, 32, 95, 46, 39, 32, 96, 45, 39, 32, 39, 46, 95, 32, 32, 32, 226, 150, 136, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 226, 150, 136, 32, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 32, 32, 226, 150, 136, 226, 150, 136, 226, 150, 136, 32, 226, 150, 136, 10, 40, 46, 45, 46, 47, 96, 45, 39, 92, 46, 45, 46, 41, 32, 32, 226, 150, 136, 10, 32, 96, 45, 39, 32, 32, 32, 32, 32, 96, 45, 39, 32, 32, 32, 226, 150, 136, 32, 32, 66, 97, 98, 121, 32, 98, 101, 97, 114, 32, 115, 97, 121, 115, 58, 32
banner_end: db 0

n1:
mov al, 1
call putbit
lodsb
dec al
jz n3
js n4

n0:
mov rsp, fuckable_stack
lodsb
cmp al, 1
jnz n2
call n1	; noreturn

n5:
and ax, 0x1336
call putbit
lodsb
test al, al
jnz n4

n8:
lodsb
push n10
push n11
shl al, 3
movzx rax, al
add rsp, rax
ret

n10:
xor al, al
mov rdi, rsi
inc rsi
scasb
jz n11
n7:
mov al, 0
call putbit
mov rdi, rsi
inc rsi
scasb
jnz n0
n13:
push qword 0
pop rax
call putbit
xor rax, rax
mov rdi, rsi
inc rsi
scasb
jz n0
n13_lodsb_for_n14:
lodsb
jmp n14_lodsb_before_calling

n2:
xor al, al
call putbit
mov rdi, banner_end
cmpsb
jnz n5
n6:
xor al, al
mov rdi, rsi
inc rsi
scasb
jz n9
jumpto8_1:
pop rdi
jmpto8_2:
pop rdx
call n8 ; noreturn

n4:
mov al, [banner+1]
mov rdi, banner_end-32
movsx rax, al
add rdi, rax
cmpsb
mov al, 0
mov dl, 1
cmovnz rax, rdx
push rax
or al, 1
call putbit
pop rax
n4n3_jmp:
mov rbx, jmpto8_2
xor rbx, n7
cmp al, 1
mov rdx, 0
cmovz rbx, rdx
xor rbx, n7
jmp rbx

n12:
mov rdx, n13
lodsb
test al, al
jnz n12_1
add rdx, n13_lodsb_for_n14-n13
n12_1:jmp rdx

n3:
lodsb
mov qword [rsp], n4n3_jmp
ret

n9:
mov al, 0
call putbit
mov al, 0
mov rdi, rsi
inc rsi
scasb
jz n12
call n11

n11:
mov rax, 1
call putbit
lodsb
cmp al, 1
jz n13
jmp n12

section UPX1 exec

_start:
	; fd = open("/dev/urandom", O_RDONLY, 0)
	mov rdx, 0	;	null flags
	mov rsi, 0	;	O_RDONLY
	mov rdi, aDevUrandom
	mov rax, 2
	syscall
	jmp x0
aDevUrandom: db "/dev/urandom", 0
	x0:
	mov rdi, rax

	; read(fd, urandom_bytes, 16)
	mov rdx, 16
	mov rsi, urandom_bytes
	mov rax, 0
	syscall

	; close(fd)
	mov rax, 3
	syscall

	; convert 16 bytes to bits
convert16:
	mov rsi, urandom_bytes
	mov rdi, urandom_bits

	binloop_bytes:
		lodsb
		mov rcx, 8
		mov dl, al
		binloop_bits:
			mov al, dl
			and al, 1
			stosb
			shr dl, 1
			loop binloop_bits

		cmp rsi, urandom_bits
		jnz binloop_bytes


	cmp byte [my_output_size], 0
	jnz skip_banner

	; write(0, 0, banner, banner_end-banner)
	mov rdx, banner_end-banner
	mov rsi, banner
	xor rdi, rdi
	mov rax, 1
	syscall

	skip_banner:

	mov byte [output_remaining], 46
	mov rsi, urandom_bits
	jmp n0

done_encoding:
	mov al, byte[your_output_size]
	cmp al, byte [my_output_size]
	jz check

	; copy moves to my_output
	mov rcx, 47
	mov rsi, your_output_size
	mov rdi, my_output_size
	rep movsb

	; clear your_output
	mov rcx, 47
	xor al, al
	mov rdi, your_output_size
	rep stosb

	; write(0, prompt, sizeof(prompt))
	mov rdx, prompt_end-prompt
	mov rsi, prompt
	xor rdi, rdi
	mov rax, 1
	syscall
	jmp x2
prompt: db 10, 10, "What do you say? "
prompt_end:
	x2:

	; reset the bits
	mov rdi, urandom_bytes
	mov al, 0
	mov rcx, 16
	rep stosb

	mov rdx, 16
	mov rsi, urandom_bytes
	mov rdi, 1
	mov rax, 0
	syscall

	jmp convert16

check:
	; write(0, think, sizeof(think))
	mov rdx, think_end-think
	mov rsi, think
	xor rdi, rdi
	mov rax, 1
	syscall
	jmp x3
think: db 10, "Baby bear is thinking...", 10
think_end:
	x3:

	mov rdi, my_output
	mov rsi, your_output
	mov rcx, 46
	repe cmpsb
	jz ok

	mov rdx, failtext_end-failtext
	mov rsi, failtext
	xor rdi, rdi
	mov rax, 1
	syscall
	jmp x5
failtext: db 10, 34, "Someone", 39, "s been eating my porridge and they ate it all up!", 34, " cried the Baby bear.", 10
failtext_end:
	x5:

	jmp done

	ok:

	mov rdx, oktext_end-oktext
	mov rsi, oktext
	xor rdi, rdi
	mov rax, 1
	syscall
	jmp x4
oktext: db 10, 34, "Yeah, that sounds like what I was thinking", 34, ", baby bear said.", 10, "Here", 39 ,"s your flag: "
oktext_end:
	x4:

	; fd = open("flag", O_RDONLY, 0)
	mov rdx, 0	;	null flags
	mov rsi, 0	;	O_RDONLY
	mov rdi, aFlag
	mov rax, 2
	syscall
	jmp x6
aFlag: db "flag", 0
	x6:
	mov rdi, rax

	; read(fd, urandom_bytes, 16)
	mov rdx, 256
	mov rsi, urandom_bytes
	mov rax, 0
	syscall

	mov rdx, rax

	; close(fd)
	mov rax, 3
	syscall

	xor rdi, rdi
	mov rax, 1
	syscall

	done:

	; exit(0)
	mov rax, 60
	mov rdi, 0
	syscall


section .bss
urandom_bytes: resb 16
urandom_bits: resb 16*8

my_output_size: resb 1
my_output: resb 48

your_output_size: resb 1
your_output: resb 48

output_remaining: resb 1

resb 256*8
fuckable_stack:
resb 256*8
